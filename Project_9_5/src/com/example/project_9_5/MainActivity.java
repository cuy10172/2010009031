package com.example.project_9_5;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
//        String output = "";
//        SensorManager sensors = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
//        List<Sensor> listSensor = sensors.getSensorList(Sensor.TYPE_ALL);
//        output = "count = " + listSensor.size() + "\n\n";
//        for (Sensor s : listSensor) {
//        output += ("name = " + s.getName() + "\n\ttype = " + s.getType() +
//        "\n\tvender = " + s.getVendor() + "\n\tversion = " + s.getVersion() +
//        "\n\tpower = " + s.getPower() + "\n\tres = " + s.getResolution() +
//        "\n\trange = " + s.getMaximumRange() + "\n\n");
//        }
//        TextView txt =(TextView)findViewById(R.id.textView1);
//        txt.setText(output);
    }
    
    @Override
    protected void onPause() {
    super.onPause();
    SensorManager sensors = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
    sensors.unregisterListener(mSensorListener);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
	    int delay = SensorManager.SENSOR_DELAY_UI;
	    SensorManager sensors = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	    sensors.registerListener(mSensorListener,
	    sensors.getDefaultSensor(Sensor.TYPE_ORIENTATION), delay);
	    sensors.registerListener(mSensorListener,
	    sensors.getDefaultSensor(Sensor.TYPE_GRAVITY), delay);
	    sensors.registerListener(mSensorListener,
	    sensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), delay);
	    sensors.registerListener(mSensorListener,
		sensors.getDefaultSensor(Sensor.TYPE_GYROSCOPE), delay);
		sensors.registerListener(mSensorListener,
		sensors.getDefaultSensor(Sensor.TYPE_LIGHT), delay);
		sensors.registerListener(mSensorListener,
		sensors.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), delay);
	}
    
	SensorEventListener mSensorListener = new SensorEventListener() {
		
		public void onAccuracyChanged(Sensor sensor, int accuracy) {}
		
		public void onSensorChanged(SensorEvent event) {
			if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
				return;
		
			String output = "";
		
			float[] v = event.values;
		
			switch (event.sensor.getType()) {
		
			case Sensor.TYPE_ORIENTATION:
				output +=
				"\n azimuth:" + v[0] +
				"\n pitch:" + v[1] +
				"\n roll:" + v[2] + "\n";
				TextView txt1 =(TextView) findViewById(R.id.editText1);
				txt1.setText(output);
				break;
			case Sensor.TYPE_GRAVITY:
				output +=
				"\n X:" + v[0] +
				"\n Y:" + v[1] +
				"\n Z:" + v[2] + "\n";
				TextView txt2 =(TextView) findViewById(R.id.editText1);
				txt2.setText(output);
				break;
			case Sensor.TYPE_ACCELEROMETER:
				output +=
				"\n X:" + v[0] +
				"\n Y:" + v[1] +
				"\n Z:" + v[2] + "\n";
				TextView txt3 =(TextView)findViewById(R.id.editText3);
				txt3.setText(output);
				break;
			case Sensor.TYPE_GYROSCOPE:
				output +=
				"\n X:" + v[0] +
				"\n Y:" + v[1] +
				"\n Z:" + v[2] + "\n";
				TextView txt4 =(TextView)findViewById(R.id.editText4);
				txt4.setText(output);
				break;
			case Sensor.TYPE_LIGHT:
				output +=
				"\n 밝기:" + v[0] +"\n";
				TextView txt5 =(TextView)findViewById(R.id.editText5);
				txt5.setText(output);
				break;
			case Sensor.TYPE_ROTATION_VECTOR:
				output +=
				"\n X:" + v[0] +
				"\n Y:" + v[1] +
				"\n Z:" + v[2] + "\n";
				TextView txt6 =(TextView)findViewById(R.id.editText6);
				txt6.setText(output);
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				output +=
				"\n 자기장:" + v[0] + "\n";
				TextView txt7 =(TextView)findViewById(R.id.editText7);
				txt7.setText(output);
			case Sensor.TYPE_TEMPERATURE:
				output +=
				"\n 온도:" + v[0] + "\n";
				TextView txt8 =(TextView)findViewById(R.id.editText8);
				txt8.setText(output);
			}
		}
	};
}
