package com.example.rose;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Random;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Rose extends Activity implements OnTouchListener {

	// 화면 크기
	WindowManager wm;
	Display display;
	
	int width;
	int height;
	
	// 데이터
	LinearLayout recordLL;
	TextView time1TV;
	TextView time2TV;
	TextView time3TV;
	Button refreshBTN;
	
	// 그래프
	LinearLayout graphLL;
	
	// 게임 진행
	ImageView bgdIV;
	ImageView playerIV;
	ImageView runner1IV;
	ImageView runner2IV;
	ImageView taggerIV;
	Button restartBTN;
	TextView taggerSpeakTV;
	TextView timerTV;
	
	RunnerPic rp1;
	RunnerPic rp2;
	TaggerPic tp;
	PlayerPic pp;
	BackgroundPic bp;
	
	int bgdNum;
	float runner1Location;
	int runner1Run;
	
	float runner2Location;
	int runner2Run;
	
	float playerLocation;
	
	int taggerIndex;
	String[] taggerStr = {"무", "궁", "화", "꽃", "이", "피", "었", "습", "니", "다", " "};
	
	int limit;
	int mainTime;
	
	int newS;
	int newMs;
	
	SettingThread settingThread;
	ImageThread imgThread;
	ResultThread resultThread;
	
	RoseRankRecord rankRecord;
	
	boolean isPlaying;
	boolean firstGame;
	
	public boolean onKeyDown( int KeyCode, KeyEvent event )
	{
		if( event.getAction() == KeyEvent.ACTION_DOWN ){
			// 이 부분은 특정 키를 눌렀을때 실행 된다.
			// 만약 뒤로 버튼을 눌럿을때 할 행동을 지정하고 싶다면	 
			if( KeyCode == KeyEvent.KEYCODE_BACK || KeyCode == KeyEvent.KEYCODE_HOME ){
				//여기에 뒤로 버튼을 눌렀을때 해야할 행동을 지정한다
				isPlaying = false;
				// return true;
				// 여기서 리턴값이 중요한데 리턴값이 true이냐 false이냐에 따라 행동이 달라진다.
				// true 일경우 back 버튼의 기본동작인 종료를 실행하게 된다.
				// 하지만 false 일 경우 back 버튼의 기본동작을 하지 않는다.
				// back 버튼을 눌렀을때 종료되지 않게 하고 싶다면 여기서 false 를 리턴하면 된다.
				// back 버튼의 기본동작을 막으면 어플리케이션을 종료할 방법이 없기때문에
				// 따로 종료하는 방법을 마련해야한다.
			}
		} 
		return super.onKeyDown( KeyCode, event );
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rose);
		
		// 화면 크기와 관련
		wm = (WindowManager)getSystemService(WINDOW_SERVICE);
	    display = wm.getDefaultDisplay();
	        
	    width = display.getWidth();
	    height = display.getHeight();
		
	    // 게임 진행과 관련
		bgdIV = (ImageView)findViewById(R.id.bgd_IV);
		playerIV = (ImageView)findViewById(R.id.player_IV);
		runner1IV = (ImageView)findViewById(R.id.runner1_IV);
		runner2IV = (ImageView)findViewById(R.id.runner2_IV);
		taggerIV = (ImageView)findViewById(R.id.tagger_IV);
		
		taggerSpeakTV = (TextView)findViewById(R.id.taggerSpeak_TV);
		
		timerTV = (TextView)findViewById(R.id.timer_TV);
		
		restartBTN = (Button)findViewById(R.id.restart_BTN);
			
		// 데어터 저장과 관련
		recordLL = (LinearLayout)findViewById(R.id.rose_record_LL);
		
		time1TV = (TextView)findViewById(R.id.rose_rankTime1_TV);
		time2TV = (TextView)findViewById(R.id.rose_rankTime2_TV);
		time3TV = (TextView)findViewById(R.id.rose_rankTime3_TV);
		
		refreshBTN = (Button)findViewById(R.id.refresh_BTN);
		
		//Log.e("ROSE", "ERROR");
		
		// 데이터 유무 체크해서 있으면 기존 데이터 부르고 없으면 새로운 파일 생성
		if(!checkFileExist())
			makeTextFile();
		loadData();
		
		// 그래프
		graphLL = (LinearLayout)findViewById(R.id.rose_graph_LL);
		
		// 초기값 세팅해주는 스레드 호출(내부에서 자동으로 게임 진행으로 넘어감) -> 이곳 수정시 시작 조절가능
		settingThread =  new SettingThread();
		settingThread.start();
		
		bgdIV.setOnTouchListener(this);
		restartBTN.setOnTouchListener(this);
		refreshBTN.setOnTouchListener(this);
	}
	
	// 이미지 조절
	private void settingImage()
	{
		if(firstGame)
		{
			Random rnd = new Random();
			
			if(runner1Run == 0)
			{
				rp1.waiting();
				runner1Run = rnd.nextInt(3);
			}
			else
			{
				rp1.run();
				runner1Run--;
				runner1Location += 4;
				if(runner1Location > limit)
				{
					playerLose();
				}
				//Log.i("ROSE", "runner1Location : "+runner1Location);
			}
			
			if(runner2Run == 0)
			{
				rp2.waiting();
				runner2Run = rnd.nextInt(3);
			}
			else
			{
				rp2.run();
				runner2Run--;
				runner2Location += 5;
				if(runner2Location > limit)
				{
					playerLose();
				}
				//Log.i("ROSE", "runner2Location : "+runner2Location);
			}
			
			pp.waiting();
			
			if(taggerIndex > taggerStr.length - 2)
			{
				tp.see();
			}
			else
			{
				tp.speak();
			}
		}
		
		playerIV.setImageBitmap(pp.getImage());
		
		runner1IV.setX(runner1Location);
		runner1IV.setImageBitmap(rp1.getImage());
		
		runner2IV.setX(runner2Location);
		runner2IV.setImageBitmap(rp2.getImage());
		
		taggerIV.setImageBitmap(tp.getImage());
		bgdNum++;
		if(bgdNum == 9)
		{
			bgdIV.setImageBitmap(bp.getImage());
			bgdNum = 0;
		}
	}
	
	// 초기화
	private void initImage()
	{
		restartBTN.setVisibility(View.INVISIBLE);
		
		rp1 = new RunnerPic(this);
		rp2 = new RunnerPic(this);
		tp = new TaggerPic(this);
		pp = new PlayerPic(this);
		bp = new BackgroundPic(this);

		isPlaying = true;
		firstGame = true;
		
		bgdNum = 0;
		runner1Run = 0;
		runner2Run = 0;
		
		taggerIndex = 0;
		taggerSpeakTV.setText("");
		
		refreshRank();
    	RelativeLayout.LayoutParams recordParam = (RelativeLayout.LayoutParams)recordLL.getLayoutParams();
        recordParam.width = width * 3 / 10;
        recordLL.setLayoutParams(recordParam);
        recordLL.setBackgroundColor(Color.BLACK);
		
		mainTime = 0;
		
		RelativeLayout.LayoutParams graphParam = (RelativeLayout.LayoutParams)graphLL.getLayoutParams();
		graphParam.width = width * 3 / 10;
		graphParam.height = width * 3 / 10;
		graphLL.setLayoutParams(graphParam);
		graphLL.setBackgroundColor(Color.BLACK);
		makeGraph();
		
		playerIV.setImageBitmap(pp.getImage());
		runner1IV.setImageBitmap(rp1.getImage());
		runner2IV.setImageBitmap(rp2.getImage());
		taggerIV.setImageBitmap(tp.getImage());
		bgdIV.setImageBitmap(bp.getImage());
		
		Log.i("Background Size", "w : "+bp.getImage().getWidth()+" / h : "+bp.getImage().getHeight());			
		
		int bpWidth = bp.getImage().getWidth();
		int bpHeight = bp.getImage().getHeight();
		
		//TODO 상대위치 적용하기
		initLocation();
		
		playerIV.setY( bgdIV.getY() + bpHeight*16/33 );
		runner1IV.setY( bgdIV.getY() + bpHeight*21/33 );
		runner2IV.setY( bgdIV.getY() + bpHeight*26/33 );
		
		taggerIV.setX( bgdIV.getX() + bpWidth*19/23 );
		taggerIV.setY( bgdIV.getY() + bpHeight*17/33 );
		
		taggerSpeakTV.setX( bgdIV.getX() + bpWidth*4/7 );
		taggerSpeakTV.setY( bgdIV.getY() + bpHeight*14/33 );
		
		limit = (int)( bgdIV.getX() + bpWidth*15/23 );
		
		imgThread = new ImageThread();
		imgThread.start();
	}
	
	// 스레드 멈추지 않고 바로 위치만 조정해서 사용할 때 쓰는 함수
	public void initLocation()
	{
		playerLocation = (int)bgdIV.getX();
		runner1Location = (int)bgdIV.getX();
		runner2Location = (int)bgdIV.getX();		
	}
	
	Handler imgHandler = new Handler(){
		
		public void handleMessage(Message msg)
		{
			//Log.i("ROSE", "Image Handler");
			
			if(msg.what == 0)
			{
				settingImage();
				taggerSpeak();
				timer();
			}
			else
			{
				initImage();
			}
		}
	};
	
	class SettingThread extends Thread
	{
		synchronized public void run()
		{
			try {
				imgHandler.sendEmptyMessage(1);
				sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
		
	class ImageThread extends Thread
	{
		synchronized public void run()
		{
			while(isPlaying)
			{
				try {
					//Log.i("ROSE", "Image Thread");
					imgHandler.sendEmptyMessage(0);
					sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}	
	}
	
	Handler resultHandler = new Handler(){
		
		public void handleMessage(Message msg)
		{
			if(msg.what == 0)
			{
				restartBTN.setVisibility(View.VISIBLE);
			}
		}
	};
	
	// 게임이 종료되고 2초 후에 재시작 버튼 화면에 생김
	class ResultThread extends Thread
	{
		synchronized public void run()
		{
			try {
				sleep(2000);
				resultHandler.sendEmptyMessage(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		//Log.i("ROSE", "Touch Event");
		
		if(v.getId() == R.id.bgd_IV && firstGame)
		{
			//Log.i("ROSE", "Touch Event : BGD_IV");
			playerRun();
			return true;	
		}
		else if(v.getId() == R.id.restart_BTN)
		{
			restartBTN.setVisibility(View.INVISIBLE);
			restartGame();
		}
		else if(v.getId() == R.id.refresh_BTN)
		{
			makeTextFile();
			refreshRank();
		}
		
		return false;
	}
	
	public void timer()
	{
		if(firstGame)
		{
			mainTime++;
	        newS = mainTime / 10;
	        newMs = mainTime % 10;
	        String minStr;
	        
	        if(newS < 10)
	        {
	        	minStr = "0"+newS;
	        }
	        else
	        {
	        	minStr = ""+newS;
	        }
	        String strTime = minStr+"."+newMs;
			timerTV.setText(strTime);
		}
	}
		
	public void taggerSpeak()
	{
		if(firstGame)
		{
			Random rnd = new Random();
			int speed = rnd.nextInt(3);
			
			if(speed == 0)
			{
				if(taggerIndex < taggerStr.length - 1)
				{
					taggerSpeakTV.setText( taggerSpeakTV.getText()+taggerStr[taggerIndex] );
					taggerIndex++;
				}
				else
				{
					taggerIndex = 0;
					taggerSpeakTV.setText("");
				}
			}
		}
	}
	
	public void playerRun()
	{
		if(taggerIndex > taggerStr.length - 2)
		{
			playerLose();
		}
		else
		{	
			pp.run();
			playerLocation += 1;
			playerIV.setX(playerLocation);
			playerIV.setImageBitmap(pp.getImage());
			//Log.i("ROSE", "playerLocation : "+playerLocation);
			
			if(playerLocation > limit)
			{
				playerWin();
			}
		}
	}
	
	public void playerWin()
	{
		stopGame();
		timeSave();
		pp.win();
		rp1.lose();
		rp2.lose();
		tp.lose();
	}
	
	public void playerLose()
	{
		stopGame();
		rp1.win();
		rp2.win();
		tp.win();
		pp.lose();
	}
	
	public void stopGame()
	{
		taggerIndex = 0;
		mainTime = 0;
		taggerSpeakTV.setText("");
		firstGame = false;
		resultThread = new ResultThread();
		resultThread.start();
	}

	public void restartGame()
	{		
		loadData();
		refreshRank();
		firstGame = true;
		pp.initGame();
		tp.speak();
		rp1.waiting();
		rp2.waiting();
		initLocation();
		playerIV.setX(playerLocation);
	}
	
	///		게임 데이터		////////////////////////////////////////////////////////////
	public void loadData()
	{
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		
		try{
			br = new BufferedReader(new InputStreamReader(openFileInput("rank.txt")));
			
			String str = null;
			
			while( (str = br.readLine()) != null )
			{
				sb.append(str + "\n");
			}
		} catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(Exception e) {
			
			try{
				br.close();
			} catch(IOException e1){
				
				e1.printStackTrace();
			}
		}
		rankRecord = new RoseRankRecord(sb.toString());
	}
	
	public void timeSave()
	{
		rankRecord.newRecord(newS, newMs);
		saveData();
	}
	
	public void saveData()
	{
		OutputStreamWriter osw = null;
		
		try{
			osw = new OutputStreamWriter(openFileOutput("rank.txt", MODE_PRIVATE));
			String str = "";
			
			for(int i = 0; i < 3; i++)
			{
				for(int j = 0; j < 2; j++)
				{
					str += rankRecord.rank[i][j]+"/";
				}
			}
			Log.i("ROSE", "String "+str);
			osw.write(str);
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try{
				osw.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void makeTextFile()
	{
		OutputStreamWriter osw = null;
		
		try{
			osw = new OutputStreamWriter(openFileOutput("rank.txt", MODE_PRIVATE));
			String str = "0/0/0/0/0/0/";
			
			Log.i("ROSE", "String "+str);
			osw.write(str);
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try{
				osw.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean checkFileExist()
	{
		String fileChk = "/data/data/com.example.rose/files/rank.txt";
		File file = new File(fileChk);

		Log.i("ROSE", "file check : "+file.exists());
		
		if(file.exists())
			return true;
		
		return false;
	}
	
	public void refreshRank()
	{
		Log.i("ROSE", "refreshRank");
		loadData();
		time1TV.setText(rankRecord.getRank1());
		time1TV.setBackgroundColor(Color.parseColor("#ffa7a7"));
		time2TV.setText(rankRecord.getRank2());
		time2TV.setBackgroundColor(Color.parseColor("#faed7d"));
		time3TV.setText(rankRecord.getRank3());
		time3TV.setBackgroundColor(Color.parseColor("#b7f0b1"));
		makeGraph();
	}
	
	// 그래프
	public void makeGraph()
	{
		graphLL.removeAllViews();
		
		float[] values = new float[3];

		values[0] = rankRecord.rank[0][0] + rankRecord.rank[0][1]/10;
		values[1] = rankRecord.rank[1][0] + rankRecord.rank[1][1]/10;
		values[2] = rankRecord.rank[2][0] + rankRecord.rank[2][1]/10;
		
		String[] verlabels = new String[] { "30", "20", "10", "0" };
		String[] horlabels = new String[] { "1st", "2nd", "3rd" };
		float max = 30.0f;
		float min = 0.0f;
		GraphView graphView = new GraphView(this, values, max, min, 10, "Time", null, verlabels, GraphView.BAR);
		
		graphLL.addView(graphView);
	}
}
