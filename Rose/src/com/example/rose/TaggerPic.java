package com.example.rose;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;

public class TaggerPic {
	
	// tagger : 0~1 speak, 2 see, 3~4 stop, 5~8 run, 9~10 lose, 11~12 win
	private final static int STATUS_SPEAK = 0;
	private final static int STATUS_SEE = 2;
	private final static int STATUS_STOP = 3;
	private final static int STATUS_RUN = 5;
	private final static int STATUS_LOSE = 9;
	private final static int STATUS_WIN = 11;

	Context context;
	
	// ?��?�??�르�?
	private Bitmap taggerPic[];
	private int cutImage = 13;

	// ?�면 ?�기
	private Display display;	
	private int width;
	private int height;
	
	// ?��?�??�기
	private int imageWidth;
	private int imageHeight;
	
	// ?��?�??�태
	private int imageTaggerNum;
	
	// 그림 그릴 좌표
	private int x;
	private int y;
	
	private int orgX;
	private int orgY;
	
	// 그림 ?�동
	int index = 0;
	private int move;
	Random rnd;
	
	// ?��?
	private boolean stop;
	
	private int runnerNum;
	
	// Constructor Start
	public TaggerPic(Context context)
	{
		this.context = context;
		taggerPic = new Bitmap[cutImage];
		runnerImageCutting();
		imageTaggerNum = STATUS_SPEAK;	
	} // Constructor End
	
	// runnerImageCutting Start
	private void runnerImageCutting()
	{
		int orgW, orgH;
		int picW, picH;
		Bitmap imgOrg;
		
		// original image
		imgOrg = BitmapFactory.decodeResource(context.getResources(), R.drawable.tagger);
		
		orgW = imgOrg.getWidth();
		orgH = imgOrg.getHeight();
		
		Log.i("TaggerPic", "orgW : "+orgW+" / orgH : "+orgH);
		
		// original image size
		picW = orgW / cutImage;
		picH = orgH;
		
		Log.i("TaggerPic", "picW : "+picW+" / picH : "+picH);
		
		
		// image cut
		for(int i = 0; i < cutImage; i++)
		{
			taggerPic[i] = Bitmap.createBitmap(imgOrg, picW * i, 0, picW, picH);
		}
		
	} // runnerImageCutting End
		
	public void imageController()
	{
		switch( imageTaggerNum )
		{
		case 0:
			imageTaggerNum++;
			break;
		case 1:
			imageTaggerNum = STATUS_SPEAK;
			break;
		case 2:
			break;	
		case 3:
			imageTaggerNum++;
			break;
		case 4:
			imageTaggerNum = STATUS_STOP;
			break;
		case 5:
		case 6:
		case 7:
			imageTaggerNum++;
			break;
		case 8:
			imageTaggerNum = STATUS_RUN;
			break;
		case 9:
			imageTaggerNum++;
			break;
		case 10:
			imageTaggerNum = STATUS_LOSE;
			break;
		case 11:
			imageTaggerNum++;
			break;
		case 12:
			imageTaggerNum = STATUS_WIN;
			break;
		}
	}
	
	public Bitmap getImage()
	{
		imageController();
		return taggerPic[imageTaggerNum];
	}
	
	public void initGame()
	{		
		imageTaggerNum = STATUS_SPEAK;
	}
	
	public void speak()
	{
		if(imageTaggerNum != STATUS_SPEAK && imageTaggerNum != STATUS_SPEAK+1)
			imageTaggerNum = STATUS_SPEAK;
	}
	
	public void see()
	{
		imageTaggerNum = STATUS_SEE;
	}
	
	public void lose()
	{
		imageTaggerNum = STATUS_LOSE;
	}
	
	public void win()
	{
		imageTaggerNum = STATUS_WIN;
	}
	
	public void run(int distance)
	{
		imageTaggerNum = STATUS_RUN;
	}
	
}