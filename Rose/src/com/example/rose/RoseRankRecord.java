package com.example.rose;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

import android.app.Application;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class RoseRankRecord {

	public int[][] rank = new int[3][2];
	
	public RoseRankRecord(String record)
	{
		StringTokenizer st = new StringTokenizer(record, "/");
		
		// 가져와서 입력
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 2; j++)
			{
				rank[i][j] = Integer.parseInt(st.nextToken());
				Log.i("ROSE", "rank["+i+"]["+j+"] = "+rank[i][j]);
			}
		}
	}
		
	public String getRank1() {
		return rank[0][0]+"."+rank[0][1];
	}
	public String getRank2() {
		return rank[1][0]+"."+rank[1][1];
	}
	public String getRank3() {
		return rank[2][0]+"."+rank[2][1];
	}
	
	public void newRecord(int newS, int newMs)
	{
		if( compare(rank[2][0], rank[2][1], newS, newMs) == 1 )
		{
			if( compare(rank[1][0], rank[1][1], newS, newMs) == 1 )
			{
				if( compare(rank[0][0], rank[0][1], newS, newMs) == 1 )
				{
					rank[2][0] = rank[1][0];
					rank[2][1] = rank[1][1];
					
					rank[1][0] = rank[0][0];
					rank[1][1] = rank[0][1];
					
					rank[0][0] = newS;
					rank[0][1] = newMs;
				}
				else if( compare(rank[0][0], rank[0][1], newS, newMs) == 0 )
				{
					rank[2][0] = rank[1][0];
					rank[2][1] = rank[1][1];
					
					rank[1][0] = newS;
					rank[1][1] = newMs;
				}
			}
			else if( compare(rank[1][0], rank[1][1], newS, newMs) == 0 )
			{
				rank[2][0] = newS;
				rank[2][1] = newMs;
			}
		}
		Log.i("ROSE", "Time Recorded "+newS+"."+newMs);		

	}
	
	private int compare(int s, int ms, int newS, int newMs)
	{
		if(s == 0 && ms == 0)
			return 1;
		else
		{
			if(s*10+ms > newS*10+newMs)
				return 1;
			if(s*10+ms == newS*10+newMs)
				return 2;
		}
		return 0;
	}

}