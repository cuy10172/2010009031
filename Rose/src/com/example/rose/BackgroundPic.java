package com.example.rose;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BackgroundPic
{
	
	private int speed;
	private int bgdSpeed;
	private int imageBgdNum;
	private Bitmap bgdPic[];
	
	Context context;
	
	private int width;
	private int height;
	
	private int bgdIndex = 0;
	
	private boolean stop = false;
	
	// Constructor Start
	public BackgroundPic(Context context)
	{	
		this.context = context;
		imageBgdNum = 0;
		bgdPic = new Bitmap[2];
		bgdImageCutting();
	} // Constructor End
	
	// bgdImageCutting Start
	private void bgdImageCutting()
	{
		int orgW, orgH;
		int picW, picH;
		Bitmap imgOrg;
		
		// original image
		imgOrg = BitmapFactory.decodeResource(context.getResources(), R.drawable.bgd);
		
		orgW = imgOrg.getWidth();
		orgH = imgOrg.getHeight();
		
		// original image size
		picW = orgW / 2;
		picH = orgH;
		
		// image cut
		for(int i = 0; i < 2; i++)
		{
			bgdPic[i] = Bitmap.createBitmap(imgOrg, picW * i, 0, picW, picH);
			//bgdPic[i] = Bitmap.createScaledBitmap(bgdPic[i], width, height, true);
		}
		
	} // bgdImageCutting End
	
	public Bitmap getImage()
	{
		imageController();
		return bgdPic[imageBgdNum];
	}
	
	public void imageController()
	{
		switch( imageBgdNum )
		{
		case 0:
			imageBgdNum++;
			break;
		case 1:
			imageBgdNum = 0;
			break;
		}
	}
}