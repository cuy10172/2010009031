package com.example.rose;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class CutBitmap {

	Bitmap[][] cutBm;
	
	Bitmap orgBm;
	
	int matrixKind;
	
	int wNum;
	int hNum;
	
	public CutBitmap(Bitmap orgBm, int wNum, int hNum)
	{
		this.orgBm = orgBm;
		this.wNum = wNum;
		this.hNum = hNum;
		
		initBitmapArray();
		cutBitmapImage();
	}
	
	private void initBitmapArray()
	{
		if(wNum > 1 && hNum > 1)	// 2차원 배열
		{
			matrixKind = 2;
			cutBm = new Bitmap[wNum][hNum];
		}
		else if(wNum == 1)			// 1차원 배열 - 세로로 자르기
		{
			matrixKind = 1;
			cutBm = new Bitmap[1][hNum];
		}
		else if(hNum == 1)			// 1차원 배열 - 가로로 자르기
		{
			matrixKind = 1;
			cutBm = new Bitmap[1][wNum];
		}
	}
		
	private void cutBitmapImage()
	{
		int orgW, orgH;
		int picW, picH;
	
		// original image size
		orgW = orgBm.getWidth();
		orgH = orgBm.getHeight();
		
		picW = orgW / wNum;
		picH = orgH / hNum;
		
		Log.i("Cut Bitmap", "orgW : "+orgW+" / orgH : "+orgH+" / picW : "+picW+" / picH : "+picH);
		
		if(hNum > 1)
		{
			int temp = wNum;
			wNum = hNum;
			hNum = temp;
		}
		
		for(int i = 0; i < hNum; i++)
		{
			for(int j = 0; j < wNum; j++)
			{
				cutBm[i][j] = Bitmap.createBitmap(orgBm, picW * j, picH * i, picW, picH);
			}
		}
	}
	
	public Bitmap[] getOneMatrixBitmapArray()
	{
		if(matrixKind == 1)
		{
			return cutBm[0];
		}
		
		return null;
	}
	
	public Bitmap[][] getTwoMatrixBitmapArray()
	{
		if(matrixKind == 2)
		{
			return cutBm;
		}
		
		return null;
	}
}
