package com.example.rose;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;

public class RunnerPic {
	
	// runner & player : 0~1 stop, 2~5 run, 6~7 stop backward, 8~11 run backward, 12~13 lose, 14~15 win
	private final static int STATUS_STOP = 0;
	private final static int STATUS_RUN = 2;
	private final static int STATUS_STOP_BACKWARD = 6;
	private final static int STATUS_RUN_BACKWARD = 8;
	private final static int STATUS_LOSE = 12;
	private final static int STATUS_WIN = 14;

	Context context;
	
	// ?��?�??�르�?
	private Bitmap runnerPic[];
	private int cutImage = 16;

	// ?�면 ?�기
	private Display display;	
	private int width;
	private int height;
	
	// ?��?�??�기
	private int imageWidth;
	private int imageHeight;
	
	// ?��?�??�태
	private int imageRunnerNum;
	
	// 그림 그릴 좌표
	private int x;
	private int y;
	
	private int orgX;
	private int orgY;
	
	// 그림 ?�동
	int index = 0;
	private int move;
	Random rnd;
	
	// ?��?
	private boolean stop;
	
	private int runnerNum;
	
	// Constructor Start
	public RunnerPic(Context context)
	{
		this.context = context;
		runnerPic = new Bitmap[cutImage];
		runnerImageCutting();
		imageRunnerNum = STATUS_STOP;	
	} // Constructor End
	
	// runnerImageCutting Start
	private void runnerImageCutting()
	{
		// original image
		Bitmap runnerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.runner);
		CutBitmap cBm = new CutBitmap(runnerImage, cutImage, 1);
		runnerPic = cBm.getOneMatrixBitmapArray();
	} // runnerImageCutting End
	
	public void imageController()
	{
		switch( imageRunnerNum )
		{
		case 0:
			imageRunnerNum++;
			break;
		case 1:
			imageRunnerNum = 0;
			break;
		case 2:
		case 3:
		case 4:
			imageRunnerNum++;
			break;
		case 5:
			imageRunnerNum = 2;
			break;
		case 6:
			imageRunnerNum++;
			break;
		case 7:
			imageRunnerNum = 6;
			break;
		case 8:
		case 9:
		case 10:
			imageRunnerNum++;
			break;
		case 11:
			imageRunnerNum = 8;
			break;
		case 12:
			imageRunnerNum++;
			break;
		case 13:
			imageRunnerNum = 12;
			break;
		case 14:
			imageRunnerNum++;
			break;
		case 15:
			imageRunnerNum = 14;
			break;
		}
	}
	
	public Bitmap getImage()
	{
		imageController();
		return runnerPic[imageRunnerNum];
	}
	
	public void initGame()
	{		
		imageRunnerNum = STATUS_STOP;
	}
	
	public void waiting()
	{
		imageRunnerNum = STATUS_STOP;
	}
	
	public void run()
	{ 
		imageRunnerNum = STATUS_RUN;
	}
	
	public void lose()
	{
		imageRunnerNum = STATUS_LOSE;
	}
	
	public void win()
	{
		imageRunnerNum = STATUS_WIN;
	}
	
}




