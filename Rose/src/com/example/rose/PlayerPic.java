package com.example.rose;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class PlayerPic{
	
	// runner & player : 0~1 stop, 2~5 run, 6~7 stop backward, 8~11 run backward, 12~13 lose, 14~15 win
	private final static int STATUS_STOP = 0;
	private final static int STATUS_RUN = 2;
	private final static int STATUS_STOP_BACKWARD = 6;
	private final static int STATUS_RUN_BACKWARD = 8;
	private final static int STATUS_LOSE = 12;
	private final static int STATUS_WIN = 14;
	
	Context context;
	
	// ?��?�??�르�?
	private Bitmap playerPic[];
	private int cutImage = 16;
	
	// ?��?�??�기
	private int imageWidth;
	private int imageHeight;
	
	// ?��?�??�태
	private int status;
	private int imagePlayerNum;
	
	// 그림 그릴 좌표
	private int x;
	private int y;
	
	private int orgX;
	private int orgY;
	
	// ?��?
	private boolean stop;
	
	public PlayerPic(Context context)
	{
		this.context = context;
		playerPic = new Bitmap[cutImage];
		imagePlayerNum = STATUS_STOP;
		playerImageCutting();		
	} // Constructor End
		
	// playerImageCutting Start
	private void playerImageCutting()
	{
		int orgW, orgH;
		int picW, picH;
		Bitmap imgOrg;
		
		// original image
		imgOrg = BitmapFactory.decodeResource(context.getResources(), R.drawable.player);
		
		orgW = imgOrg.getWidth();
		orgH = imgOrg.getHeight();
		
		Log.i("PlayerPic", "orgW : "+orgW+" / orgH : "+orgH);
		
		// original image size
		picW = orgW / cutImage;
		picH = orgH;
		
		Log.i("PlayerPic", "picW : "+picW+" / picH : "+picH);
		
		// image cut
		for(int i = 0; i < cutImage; i++)
		{
			playerPic[i] = Bitmap.createBitmap(imgOrg, picW * i, 0, picW, picH);
			//playerPic[i] = Bitmap.createScaledBitmap(playerPic[i], imageWidth, imageHeight, true);
		}
		
	} // playerImageCutting End
	
	public Bitmap getImage()
	{
		imageController();
		return playerPic[imagePlayerNum];
	}
	
	public void imageController()
	{
		switch( imagePlayerNum )
		{
		case 0:
			imagePlayerNum++;
			break;
		case 1:
			imagePlayerNum = 0;
			break;
		case 2:
		case 3:
		case 4:
			imagePlayerNum++;
			break;
		case 5:
			imagePlayerNum = 2;
			break;
		case 6:
			imagePlayerNum++;
			break;
		case 7:
			imagePlayerNum = 6;
			break;
		case 8:
		case 9:
		case 10:
			imagePlayerNum++;
			break;
		case 11:
			imagePlayerNum = 8;
			break;
		case 12:
			imagePlayerNum++;
			break;
		case 13:
			imagePlayerNum = 12;
			break;
		case 14:
			imagePlayerNum++;
			break;
		case 15:
			imagePlayerNum = 14;
			break;
		}
	}
	
	public void initGame()
	{	
		imagePlayerNum = STATUS_STOP;
	}
	
	public void waiting()
	{
		if(imagePlayerNum > 1 && imagePlayerNum < 6)
			imagePlayerNum = STATUS_STOP;
	}
	
	public void run()
	{
		if(imagePlayerNum > 5 || imagePlayerNum < 2)
			imagePlayerNum = STATUS_RUN;
	}
	
	public void runBack()
	{
		imagePlayerNum = STATUS_STOP_BACKWARD;
	}
	
	public void lose()
	{
		imagePlayerNum = STATUS_LOSE;
	}
	
	public void win()
	{
		imagePlayerNum = STATUS_WIN;
	}

}
