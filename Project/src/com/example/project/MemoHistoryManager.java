package com.example.project;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class MemoHistoryManager {

	static final private int ARRAY_SIZE = 10;
	
	static private Context context;
	static private SharedPreferences sharedPref;
	static private SharedPreferences.Editor editor;
	
	static private String[] memoArray;
	static private String[][] positionArray;

	static private void makeSettingsManager(Context c)
	{
		context = c;
		sharedPref = c.getSharedPreferences("Search Memo", Context.MODE_PRIVATE);
		editor = sharedPref.edit();		
	}
	
	static private void setHistorySize(int historySize)
	{
		editor.putInt("Search Memo Size", historySize);
		editor.commit();
	}
	
	static public int getMemoSize(Context c)
	{
		makeSettingsManager(c);
		int historySize = sharedPref.getInt("Search Memo Size", 0);
		return historySize;
	}
	
	static public void setMemoList(Context c, ArrayList<Memo> memos)
	{
        makeSettingsManager(c);

		String memoKey = "";
		String memo = "";

		int memoSize = memos.size();
		
		for(int i = 0; i < memoSize; i++)
		{
			memoKey = "Search Memo "+i;
			
			// 책 정보 하나의 String으로 합치기
            try {
            	memo = MemoParsing.toString( memos.get(i) );
                Log.i("MSG", " Encoded serialized version " );
                Log.i("MSG", memo );
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

			// 책 정보 저장
			editor.putString(memoKey, memo);
			editor.commit();
		}

        setHistorySize(memoSize);
	}

	static public ArrayList<Memo> getMemoList(Context c)
	{
        makeSettingsManager(c);

		ArrayList<Memo> memos = new ArrayList<Memo>();
		
		// 저장된 크기 가져오기
		int memoSize = getMemoSize(c);
		
		String memoKey = "";
		
		for(int i = 0; i < memoSize; i++)
		{
			memoKey = "Search Memo "+i;
			String memoString = sharedPref.getString(memoKey, "");

			Memo memo = null;
            try {
                memo = ( Memo ) MemoParsing.fromString( memoString );
                memos.add(memo);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
		}
		return memos;
	}
	
	static public Memo getMemo(Context c, int memoIndex)
	{
        makeSettingsManager(c);

		String memoKey = "";
		memoKey = "Search Memo "+memoIndex;
		
		String memoString = sharedPref.getString(memoKey, "");

		Memo memo = null;
        try {
            memo = ( Memo ) MemoParsing.fromString( memoString );
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return memo;
	}

    static public void addMemo(Context c, Memo newMemo)
    {
        makeSettingsManager(c);

        // 불러오기
        ArrayList<Memo> memos = getMemoList(c);

        boolean duplicate = false;

        // 중복 처리
        for (Memo memo : memos)
        {
            if ( memo.getMessage().compareTo(newMemo.getTitle()) == 0)
            {
            	memos.remove(memo);
                duplicate = true;
                break;
            }
        }

        // 히스토리 개수 처리
        if(!duplicate && memos.size() > ARRAY_SIZE - 1)
        {
        	memos.remove(0);
        }

        // 추가
        memos.add(newMemo);

        // 저장
        setMemoList(c, memos);
    }
    
    static public void deleteMemo(Context c, int memoIndex)
    {
        makeSettingsManager(c);

        // 불러오기
        ArrayList<Memo> memos = getMemoList(c);

        memos.remove(memoIndex);

        // 저장
        setMemoList(c, memos);
    }
}
