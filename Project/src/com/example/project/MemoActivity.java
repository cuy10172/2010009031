package com.example.project;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MemoActivity extends Activity {
	
	EditText titleET;
	EditText messageET;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo);
        
        titleET = (EditText)findViewById(R.id.memo_title_et);
        messageET = (EditText)findViewById(R.id.memo_message_et);
        
        titleET.setText(getIntent().getStringExtra("Title"));
        messageET.setText(getIntent().getStringExtra("Message"));
        
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.memo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.save_memo) {
        	MemoHistoryManager.addMemo(this, new Memo(titleET.getText().toString(), messageET.getText().toString(), ""));
        	finish();
        	return true;
        }
        else if (id == R.id.delete_memo) {
        	MemoHistoryManager.deleteMemo(this, getIntent().getIntExtra("Index", -1));
        	finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
