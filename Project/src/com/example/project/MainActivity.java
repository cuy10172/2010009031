package com.example.project;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

	private RelativeLayout displayRL;
	private ListView memoLV;	
	private MemoAdapter adapter;
	private LayoutInflater mInflater;
	private ArrayList<Memo> memoList;
	private Context context;
	private TextView title;
	private TextView date;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        displayRL = (RelativeLayout)findViewById(R.id.display_rl);
        memoLV = (ListView)findViewById(R.id.list);
        
		memoLV = (ListView) findViewById(R.id.list);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        memoList = new ArrayList<Memo>();
        
        adapter = new MemoAdapter(this, memoList);
		memoLV.setAdapter(adapter);
		context = this;
		
		for(int i = 0; i < MemoHistoryManager.getMemoSize(this); i++)
		{
			adapter.add(MemoHistoryManager.getMemo(this, i));
		}
		
		memoLV.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position,
					long id) {	
				Memo memo = (Memo)adapter.getAdapter().getItem(position);
				String title = memo.getTitle();
				
				Intent intent = new Intent(MainActivity.this, MemoActivity.class);
				intent.putExtra("Title", memo.getTitle());
				intent.putExtra("Message", memo.getMessage());
				intent.putExtra("Index", position);
				startActivity(intent);
				//Toast.makeText(adapter.getContext(), title, Toast.LENGTH_SHORT).show();
			}
		});
    }
    
//    public Memo makeMemo(String title, String message, String date)
//    {
//    	Memo m = new Memo();
//    	m.title = title;
//    	m.message = message;
//    	m.date = date;
//    	
//    	return m;
//    }
    
	public class MemoAdapter extends ArrayAdapter<Memo> {
		private LayoutInflater mInflater;
		
		public MemoAdapter(Context context, ArrayList<Memo> object) {
			super(context, 0, object);	
			
			mInflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			
			View view = null;
			
			if (v == null) {
				view = mInflater.inflate(R.layout.custom_view, null);
			} else {
				view = v;
			}
			
			final Memo memo = this.getItem(position);
			
			if (memo != null) {
				title = (TextView) view.findViewById(R.id.tv1);
				date = (TextView) view.findViewById(R.id.tv2);
				title.setText(memo.getTitle());
				date.setText(memo.getDate());
			}
			return view;
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add_memo) {
        	Intent intent = new Intent(MainActivity.this, MemoActivity.class);
			intent.putExtra("Title", "Title");
			intent.putExtra("Message", "Message");
			startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
