package com.example.project;

import java.io.Serializable;

import android.content.Context;

public class Memo implements Serializable {
	
	private String title = "";
	private String message = "";
	private String date = "";
	
	public Memo(String title, String message, String date)
	{
		setTitle(title);
		setMessage(message);
		setDate(date);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
