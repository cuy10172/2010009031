package com.example.practice_5_1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    
        //printRawFile();
        
        
        
    }
    
    private void printRawFile()
    {
      try 
      { 
      	InputStream is = getResources().openRawResource(R.raw.rawtext);
      	byte[] data = new byte[is.available()];
      	while (is.read(data) != -1) 
      	{
      		;
      	} 
      	is.close(); 
      	
      	//TextView txt = (TextView)findViewById(R.id.textView1);
      	//txt.setText(new String(data)); 
  	} catch (IOException e) { 
  		e.printStackTrace();
		}
    }
    
    public void onButtonClick(View v) 
    { 
    	Toast msg = null; 
    	switch (v.getId()) 
    	{
    	case R.id.button1:
    		//msg = Toast.makeText(this, "파일 생성", Toast.LENGTH_SHORT);
    		msg = makeFile();
    		break; 
		case R.id.button2: 
			//msg = Toast.makeText(this, "파일 읽기", Toast.LENGTH_SHORT); 
			msg = loadFile();
			break; 
		case R.id.button3: 
			//msg = Toast.makeText(this, "파일 삭제", Toast.LENGTH_SHORT); 
			msg = removeFile();
			break; 
		case R.id.button4: 
			//msg = Toast.makeText(this, "파일 목록", Toast.LENGTH_SHORT); 
			msg = showFile();
			break; 
		}
    	
    	msg.show(); 
	}
    
    private Toast makeFile()
    {
    	Toast msg;
    	
    	EditText filename = (EditText)findViewById(R.id.editText0); 
    	EditText txt = (EditText)findViewById(R.id.editText1); 
    	String input = txt.getText().toString(); 
    	try 
    	{ 
    		FileOutputStream fos = openFileOutput(filename.getText().toString()+".txt", Context.MODE_WORLD_READABLE); 
    		fos.write(input.getBytes()); 
    		fos.close(); 
		} catch (FileNotFoundException e) {
			msg = Toast.makeText(this, "파일 생성 실패\n파일을 찾을 수 없음", Toast.LENGTH_SHORT); 
			e.printStackTrace(); 
			return msg;	 
		} catch (IOException e) {
			msg = Toast.makeText(this, "파일 생성 실패\n내용을 쓸 수 없음", Toast.LENGTH_SHORT);
			e.printStackTrace(); 
			return msg;
		} 
    	
    	msg = Toast.makeText(this, "파일 생성 성공", Toast.LENGTH_SHORT); 
    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
    	imm.hideSoftInputFromWindow(findViewById(R.id.editText1).getWindowToken(), 0);
    	return msg;
    }
    
    private Toast loadFile()
    {
    	Toast msg;
    	EditText filename = (EditText)findViewById(R.id.editText0);
    	
    	TextView view = (TextView)findViewById(R.id.textView1); 
    	try 
    	{ 
    		FileInputStream fis = openFileInput(filename.getText().toString()+".txt"); 
    		byte[] buff = new byte[fis.available()]; 
    		while (fis.read(buff) != -1) {;} 
    		fis.close(); 
    		view.setText(new String(buff)); 
		} catch (FileNotFoundException e) {
			msg = Toast.makeText(this, "파일 읽기 실패\n파일을 찾을 수 없음", Toast.LENGTH_SHORT);
			e.printStackTrace(); 
			return msg;
		} catch (IOException e) { 
			msg = Toast.makeText(this, "파일 읽기 실패\n내용을 읽을 수 없음", Toast.LENGTH_SHORT); 
			e.printStackTrace(); 
			return msg;
		} 
    	
    	msg = Toast.makeText(this, "파일 읽기 성공", Toast.LENGTH_SHORT); 
    	return msg;
    }
    
    private Toast removeFile()
    {
    	Toast msg;
    	EditText filename = (EditText)findViewById(R.id.editText0);
    	
    	if (deleteFile(filename.getText().toString()+".txt")) 
    	{ 
    		msg = Toast.makeText(this, "파일 삭제 성공", Toast.LENGTH_SHORT); 
		} else { 
			msg = Toast.makeText(this, "파일 삭제 실패", Toast.LENGTH_SHORT); 
		} 
    	
    	return msg;
	}
    
    private Toast showFile()
    {
    	Toast msg;
    	
    	String output = "파일 목록";
    	String[] list = fileList(); 
    	output += ": 파일 개수 " + list.length + "개";
    	for (String file : list) {
    		output += "\n"; output += file; 
		} 
    	
    	TextView view2 = (TextView)findViewById(R.id.textView2); 
    	view2.setText(output); 
    	msg = Toast.makeText(this, "파일 목록 확인 성공", Toast.LENGTH_SHORT);
    	
    	return msg;
    }
}
