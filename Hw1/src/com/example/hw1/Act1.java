package com.example.hw1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act1 extends Activity {

	Button btnGo2, btnGo3;
	
    @Override
	protected void onStart() {
		Log.i("Act1", "onStart");
    	super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i("Act1", "onRestart");
    	super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i("Act1", "onResume");
    	super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i("Act1", "onPause");
    	super.onPause();
	} 

	@Override
	protected void onStop() {
		Log.i("Act1", "onStop");
    	super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i("Act1", "onDestroy");
    	super.onDestroy();
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		Log.i("Act1", "������");
		Log.i("Act1", "onCreate");
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);
        
        btnGo2 = (Button)findViewById(R.id.btn_act1_go2);
        btnGo2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act1.this, Act2.class);
				startActivity(intent);
			}
		});
        
        btnGo3 = (Button)findViewById(R.id.btn_act1_go3);
        btnGo3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act1.this, Act3.class);
				startActivity(intent);
			}
		});
        
    }
}
