package com.example.hw1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class Act3 extends Activity {

	Button btnGo1, btnGo2, btnGo4;
	
    @Override
	protected void onStart() {
		Log.i("Act3", "onStart");
    	super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i("Act3", "onRestart");
    	super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i("Act3", "onResume");
    	super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i("Act3", "onPause");
    	super.onPause();
	} 

	@Override
	protected void onStop() {
		Log.i("Act3", "onStop");
    	super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i("Act3", "onDestroy");
    	super.onDestroy();
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		//Log.i("Act3", "������");
		Log.i("Act3", "onCreate");
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3);
        
        btnGo1 = (Button)findViewById(R.id.btn_act3_go1);
        btnGo1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act3.this, Act1.class);
				startActivity(intent);
			}
		});
        
        btnGo2 = (Button)findViewById(R.id.btn_act3_go2);
        btnGo2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act3.this, Act2.class);
				startActivity(intent);
			}
		});
        
        btnGo4 = (Button)findViewById(R.id.btn_act3_go4);
        btnGo4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act3.this, Act4.class);
				startActivity(intent);
			}
		});
    }
}
