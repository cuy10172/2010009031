package com.example.hw1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class Act2 extends Activity {

	Button btnGo1, btnGo3, btnGo4;
	
    @Override
	protected void onStart() {
		Log.i("Act2", "onStart");
    	super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i("Act2", "onRestart");
    	super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i("Act2", "onResume");
    	super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i("Act2", "onPause");
    	super.onPause();
	} 

	@Override
	protected void onStop() {
		Log.i("Act2", "onStop");
    	super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i("Act2", "onDestroy");
    	super.onDestroy();
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		//Log.i("Act2", "������");
		Log.i("Act2", "onCreate");
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2);
        
        btnGo1 = (Button)findViewById(R.id.btn_act2_go1);
        btnGo1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act2.this, Act1.class);
				startActivity(intent);
			}
		});
        
        btnGo3 = (Button)findViewById(R.id.btn_act2_go3);
        btnGo3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act2.this, Act3.class);
				startActivity(intent);
			}
		});
        
        btnGo4 = (Button)findViewById(R.id.btn_act2_go4);
        btnGo4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Act2.this, Act4.class);
				startActivity(intent);
			}
		});
    }
}
