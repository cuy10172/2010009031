package com.example.ex4_intent2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class Act1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);
    }
    
    public void onClickButton(View v)
    {
    	Intent intent1 = null;
    	
    	switch(v.getId())
    	{
    	case R.id.act1_dial_btn:
    		intent1 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0539408695"));
    		break;
    	case R.id.act1_contacts_btn:
    		intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
    		break;
    	case R.id.act1_geo_btn:
    		intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:37.58,126.98"));
    		break;
    	case R.id.act1_url_btn:
    		intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/"));
    		break;
    	case R.id.act1_view_btn:
    		intent1 = new Intent(Intent.ACTION_VIEW);
    		break;
    	case R.id.act1_insert_or_edit_btn:
    		intent1 = new Intent(Intent.ACTION_INSERT_OR_EDIT, Uri.parse("content://contacts/people/1"));
    		break;
    	}
    	
    	startActivity(intent1);
    }
}
