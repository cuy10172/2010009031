package com.example.practice_9_4;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MainActivity extends FragmentActivity {
	
	private GoogleMap googleMap;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        init();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        init();
    }
    
    void init()
    {
    	if(googleMap == null)
    	{
    		googleMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.fragment1)).getMap();
    		if(googleMap!=null)
    		{
    			addMarker();
    		}
    	}
    	googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(35.886869,  128.608408), 16));
    }
    
    private void addMarker()
    {
    	googleMap.addMarker(new MarkerOptions().position(
    			new LatLng(35.886869,  128.608408)).title("School"));
    	
    	MarkerOptions shopMarker []= {
    			new MarkerOptions().position ( new LatLng
    			(35.888345, 128.609668)).title("shop").snippet("Chicken"),
    			new MarkerOptions().position ( new LatLng
    			(35.886971, 128.609196)).title("shop2"),
    			new MarkerOptions().position ( new LatLng
    			(35.891792, 128.610631)).title("shop3")
			};
		for(int i = 0; i<3 ; ++i){
			shopMarker[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher));
			googleMap.addMarker(shopMarker[i]);
		}
    }
}
